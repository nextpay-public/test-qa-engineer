import React, { useState, useEffect } from "react";
import _ from "lodash";
import {
  Container,
  Box,
  Paper,
  Grid,
  TextField,
  InputAdornment,
  Button,
  MenuItem,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableContainer,
  TableRow,
} from "@material-ui/core";
import { Alert } from "@material-ui/lab";
import { DirectionsBoat, FitnessCenter, HomeWork } from "@material-ui/icons";

const App = () => {
  const [weight, setWeight] = useState("");
  const [homePort, setHomePort] = useState("");
  const [shipType, setShipType] = useState(-1);
  const [ships, setShips] = useState([]);

  const shipTypes = [
    {
      value: -1,
      label: "-- Select ship type --",
    },
    {
      value: "Tug",
      label: "Tug",
    },
    {
      value: "Cargo",
      label: "Cargo",
    },
    {
      value: "Barge",
      label: "Barge",
    },
    {
      value: "High Speed Craft",
      label: "High Speed Craft",
    },
  ];

  const fetchShips = () => {
    fetch("http://localhost:4000/ships")
      .then((response) => response.json())
      .then(({ result }) => {
        let filteredShips = result;

        if (shipType !== -1) {
          filteredShips = _.filter(filteredShips, { ship_type: shipType });
        }

        if (!_.isEmpty(homePort)) {
          filteredShips = _.filter(filteredShips, (ship) => {
            return ship.home_port
              .toLowerCase()
              .includes(homePort.toLowerCase());
          });
        }

        if (!_.isEmpty(weight)) {
          filteredShips = _.filter(filteredShips, {
            weight_kg: parseInt(weight),
          });
        }

        setShips(filteredShips);
      });
  };

  useEffect(() => {
    document.title = "Flight Dashboard";
    fetchShips();
    // eslint-disable-next-line
  }, []);

  return (
    <Box component="section" py={8}>
      <Container maxWidth="lg">
        <Grid container spacing={2}>
          <Grid item md={3}>
            <Box mb={2}>
              <TextField
                label="Ship type"
                select
                variant="outlined"
                value={shipType}
                fullWidth
                onChange={({ target: { value } }) => {
                  setShipType(value);
                }}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <DirectionsBoat />
                    </InputAdornment>
                  ),
                }}
              >
                {shipTypes.map((option) => (
                  <MenuItem key={option.value} value={option.value}>
                    {option.label}
                  </MenuItem>
                ))}
              </TextField>
            </Box>
            <Box mb={2}>
              <TextField
                label="Weight"
                variant="outlined"
                value={weight}
                fullWidth
                onChange={({ target: { value } }) => {
                  setWeight(value);
                }}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <FitnessCenter />
                    </InputAdornment>
                  ),
                }}
              />
            </Box>
            <Box mb={2}>
              <TextField
                label="Home port"
                variant="outlined"
                value={homePort}
                fullWidth
                onChange={({ target: { value } }) => {
                  setHomePort(value);
                }}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <HomeWork />
                    </InputAdornment>
                  ),
                }}
              />
            </Box>
            <Button variant="contained" color="primary" onClick={fetchShips}>
              Search
            </Button>
          </Grid>
          <Grid item md={9}>
            {_.isEmpty(ships) ? (
              <Alert severity="error">There are no ships available.</Alert>
            ) : (
              <TableContainer component={Paper}>
                <Table stickyHeader>
                  <TableHead>
                    <TableRow>
                      <TableCell>Ship Type</TableCell>
                      <TableCell>Weight</TableCell>
                      <TableCell>Home port</TableCell>
                      <TableCell>Ship name</TableCell>
                      <TableCell>Class</TableCell>
                      <TableCell>Image</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {ships.map((ship, index) => {
                      const {
                        home_port,
                        ship_type,
                        weight_kg,
                        ship_name,
                        image,
                        class: ship_class,
                      } = ship;
                      return (
                        <TableRow key={index}>
                          <TableCell>{ship_type}</TableCell>
                          <TableCell>{weight_kg}</TableCell>
                          <TableCell>{home_port}</TableCell>
                          <TableCell>{ship_name}</TableCell>
                          <TableCell>{ship_class}</TableCell>
                          <TableCell>
                            <Box
                              component="img"
                              src={image}
                              width="200px"
                              borderRadius={8}
                            />
                          </TableCell>
                        </TableRow>
                      );
                    })}
                  </TableBody>
                </Table>
              </TableContainer>
            )}
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
};

export default App;
