# QA Engineer Skills Assessment

#### Task Description: Your squad has been assigned to develop a dashboard to display SpaceX data as described below. Read the requirements and come up with a strategy on how you would test the UI and validate the API.

---

### UI Specifications

- A basic html to show data returned from the backend API in a tabular format.
- It can have pagination.
- Ability to filter data based on attributes like ship type (dropdown), weight (numeric), home port (string).
- The control console (the buttons container) should be positioned just above the table as shown in image below.
- The control console should provide a button that fetches All ships.
- Should provide a button for each row to allow the engineers to upload ship icon/image (only jpg/jpeg,png allowed and upto size 100kb).
- Add validation for the input fields: data type, range, select list.

See UI mock below

![UI Mock](ui-mock-simple.png)

## Getting Started

You will not be performing the actual tests on the completed version of the app. Instead, you have a boilerplate code for the unfinished version.

The developers are still writing the code but you can already do the prep work and setup your testing tool of choice to create very minimal system tests.

To run the code, you need to have Docker running on your machine. If you do not, they offer easy to install binaries ([Mac](https://docs.docker.com/docker-for-mac/install/)) ([Windows](https://docs.docker.com/docker-for-windows/install/)).

From the docker folder of the project, run `docker-compose up -d`

- You should now have the UI running at http://localhost:3000
- API can be accessed via GET http://localhost:4000/ships

## Minimum Expected Deliverables

### Output will be presented during the interview. No need to submit anything.

1.  Test Cases. Show your test plan with the different test cases you have identified based on the `UI Specifications`.
2.  Using the boilerplate code provided and whatever is available on the unfinished app, write automated system tests for the app. Make at least:
    - 1 for UI
    - 1 for the endpoint `(GET /ships)`

---

#### Alternative way to run the code

**If you can't run the app via Docker, do these instead:**

1. Run `npm install` in both `front-end` and `server` folders.
2. In the `server` folder, please run `node app`.
3. Open another tab and change directory to the `front-end` folder. Afterwards, run `npm start`.
